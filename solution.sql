/*Create an solution.sql file inside s04/a1 project and do the following using the music_db database:
1. Find all artists that has letter d in its name.
2. Find all songs that has a length of less than 3:50.
3. Join the 'albums' and 'songs' tables. (Only show the album name, song name, and song length.)
4. Join the 'artists' and 'albums' tables. (Find all albums that has letter a in its name.)
5. Sort the albums in Z-A order. (Show only the first 4 records.)
6. Join the 'albums' and 'songs' tables. (Sort albums from Z-A)*/

SELECT * FROM artists WHERE name LIKE '%d%';

SELECT * FROM songs WHERE length < 350;

SELECT albums.album_title, songs.song_name, songs.length
	FROM albums
	JOIN songs ON albums.id = songs.album_id;

SELECT * FROM artists
	JOIN albums ON artists.id = albums.artist_id
	WHERE albums.album_title LIKE '%a%';

SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

SELECT * FROM albums
	JOIN songs ON albums.id = songs.album_id 
	ORDER BY album_title DESC;
